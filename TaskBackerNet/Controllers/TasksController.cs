﻿using System;
using System.Collections.Generic;
using System.Data.SQLite;
using System.IO;
using System.Net;
using System.Threading.Tasks;
using System.Web.Http;
using TaskBackerNet.Classes;

namespace TaskBackerNet.Controllers
{
	public class TasksController : ApiController
	{
		private string _connectionString;

		public TasksController()
		{
			SetupDb();
		}

		public List<TaskItem> Get(int perPage = 10, int currentPage = 0)
		{
			var taskList = new List<TaskItem>();
			using (var sqLite = new SQLiteConnection(_connectionString))
			{
				sqLite.Open();
				using (var command = new SQLiteCommand(string.Format("SELECT * FROM TaskList LIMIT {0} OFFSET {1}", perPage, currentPage * perPage), sqLite))
				{
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
							taskList.Add(new TaskItem(int.Parse(reader["Id"].ToString()), reader["Name"].ToString(), reader["Task"].ToString()));
					}
				}
			}

			return taskList;
		}

		public List<TaskItem> Get(string name)
		{
			var taskList = new List<TaskItem>();
			using (var sqLite = new SQLiteConnection(_connectionString))
			{
				sqLite.Open();
				using (var command = new SQLiteCommand(string.Format("SELECT * FROM TaskList WHERE Name = '{0}'", name), sqLite))
				{
					using (var reader = command.ExecuteReader())
					{
						while (reader.Read())
							taskList.Add(new TaskItem(int.Parse(reader["Id"].ToString()), reader["Name"].ToString(), reader["Task"].ToString()));
					}
				}
			}

			return taskList;
		}

		public async Task<HttpStatusCode> Post(string name)
		{
			var body = await Request.Content.ReadAsStringAsync();
			using (var sqLite = new SQLiteConnection(_connectionString))
			{
				sqLite.Open();
				using (var command = new SQLiteCommand(string.Format("INSERT INTO TaskList (Name,Task) VALUES ('{0}','{1}')", name, body), sqLite))
				{
					command.ExecuteNonQuery();
				}
			}

			return HttpStatusCode.Created;
		}

		public HttpStatusCode Delete(string id)
		{
			using (var sqLite = new SQLiteConnection(_connectionString))
			{
				sqLite.Open();
				using (var command = new SQLiteCommand(string.Format("DELETE FROM TaskList WHERE Id = '{0}'", id), sqLite))
				{
					command.ExecuteNonQuery();
				}
			}

			return HttpStatusCode.OK;
		}

		public void Options()
		{
			
		}

		private void SetupDb()
		{
			var tasksFolder = Path.Combine(Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData), "Tasks");
			var tasksFile = Path.Combine(tasksFolder, "Tasks.db");
			_connectionString = "Data Source=" + tasksFile + ";Version=3;";

			if (!Directory.Exists(tasksFolder))
				Directory.CreateDirectory(tasksFolder);

			if (!File.Exists(tasksFile))
				SQLiteConnection.CreateFile(tasksFile);

			using (var sqLite = new SQLiteConnection(_connectionString))
			{
				sqLite.Open();
				using (var command = new SQLiteCommand("CREATE TABLE IF NOT EXISTS TaskList (Id INTEGER PRIMARY KEY AUTOINCREMENT, Name TEXT, Task TEXT)", sqLite))
				{
					command.ExecuteNonQuery();
				}
			}
		}
	}
}
