﻿namespace TaskBackerNet.Classes
{
	public class TaskItem
	{
		public int Id { get; private set; }
		public string Name { get; private set; }
		public string Task { get; private set; }

		public TaskItem(int id, string name, string task)
		{
			Id = id;
			Name = name;
			Task = task;
		}
	}
}