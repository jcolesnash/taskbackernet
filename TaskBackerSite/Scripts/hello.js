﻿function TaskListHeader() {
	$(".TaskTable").append("<tr>");
	$(".TaskTable").append("<th>Id</th>");
	$(".TaskTable").append("<th>Name</th>");
	$(".TaskTable").append("<th>Task</th>");
	$(".TaskTable").append("<th>Delete</th>");
	$(".TaskTable").append("</tr>");
}

$(document).ready(function () {
	var grid = $("#jqGrid").jqGrid({
		onSelectRow: function(id){
			var rowData = $("#jqGrid").getRowData(id);
			var id = rowData['Id'];

			$("#jqGrid").setGridParam("editurl", "http://localhost:64935/api/Tasks?Name=" + id);
		},
		url: "http://localhost:51592/api/Tasks",
		datatype: "json",
		colNames: ["Id", "Name", "Task"],
		colModel: [
			{ name: "Id", index: "Id", key: true, width: 50 },
			{ name: "Name", index: "Name", width: 100 },
			{ name: "Task", index: "Task", width: 350 }
		],
		prmNames: [
			{ page: "currentPage" },
			{ rows: "perPage" }
		],
		pager: "#pager",
		sortname: "Id",
		sortorder: "asc",
		height: "100%",
		caption: "Task List",
		addurl: "http://localhost:51592/api/Tasks?=Name=",
		//postData: {
		//	mySearch: function () { return $('#fab').val(); }
		//},
		delurl: "amazing"
	});
	grid.jqGrid(
		"navGrid", "#pager",
		{ add: true, edit: false, del: true, search: false, refresh: true },
		{ url: "http://localhost:64935/api/Tasks?Name=" },//Edit
		{ url: "http://localhost:64935/api/Tasks?Name=" },//Add
		{ url: "http://localhost:64935/api/Tasks?Name=" },//Delete
		{ multipleSearch: true, multipleGroup: true, showQuery: true });
});